﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshRenderer))]
public class OceanController : MonoBehaviour
{
    public static OceanController instance;

    public GameObject waverPrefab, surferPrefab;

    [HideInInspector]
    public Waver oldestWaver;
    [HideInInspector]
    public List<Waver> totalWavers;
    [HideInInspector]
    public Mesh mesh;
    [HideInInspector]
    public Vector3[] originalVerticies, currentVerticies;
    [HideInInspector]
    public Vector2[] uvs;
    [HideInInspector]
    public int xSize, zSize, currentActiveSurfer;
    
    public WaverProperties currentWaverProperties;
    public CurrenOceanProperties currenOceanProperties;
    
    int[] originalTriangles, currentTriangles;
    
    protected MeshRenderer meshRenderer;
    protected MeshFilter meshFilter;
    Material thisMaterial;

    private void Awake() {

        instance = this;
    }

    private void OnEnable() {

        OceanManager.OnPurchaseUpdateDone += RecalculateOceanProperties;

    }

    private void OnDisable() {

        OceanManager.OnPurchaseUpdateDone -= RecalculateOceanProperties;
    }

    public void RecalculateOceanProperties(BaseOceanProperties baseOceanProperties) {

        currenOceanProperties = new CurrenOceanProperties(baseOceanProperties);
        currentWaverProperties = currenOceanProperties.waverProperties;
        currenOceanProperties.numberOfSurfer = 1 + currenOceanProperties.numberOfUpgrade / 2;

    }

    public void InitializeOcean(BaseOceanProperties baseOceanProperties)
    {
        RecalculateOceanProperties(baseOceanProperties);
        xSize = currenOceanProperties.xSize;
        zSize = currenOceanProperties.zSize;

        mesh = new Mesh {
            name = gameObject.name
        };
        meshRenderer = GetComponent<MeshRenderer>();
        thisMaterial = meshRenderer.material;
        thisMaterial.SetTextureScale("_MainTex", new Vector2(xSize, zSize));
        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        originalVerticies = GetVerticies();
        originalTriangles = GetTriangles();
        uvs = GetUVs();

        currentVerticies = new Vector3[originalVerticies.Length];
        for (int i = 0; i < originalVerticies.Length; i++) {

            currentVerticies[i].x = originalVerticies[i].x;
            currentVerticies[i].y = originalVerticies[i].y;
            currentVerticies[i].z = originalVerticies[i].z;
        }
        currentTriangles = originalTriangles;

        mesh.vertices = currentVerticies;
        mesh.triangles = currentTriangles;
        mesh.uv = uvs;

        StartCoroutine(StartWaving());
        StartCoroutine(StartAddingSurfer());

    }

    IEnumerator StartAddingSurfer() {

        yield return new WaitForSeconds(Random.Range(2, 5));

        if (currentActiveSurfer < currenOceanProperties.numberOfSurfer) {

            GameObject go = Utility_Manager.instance.objectPoolManager.InstantiateFromPooler(surferPrefab);
            SurferController surferController = go.GetComponent<SurferController>();
            go.SetActive(true);
            OceanObjects.instance.AddFlotingObject(go.transform);
            surferController.Reset();
            currentActiveSurfer++;
        }
        StartCoroutine(StartAddingSurfer());
    }

    IEnumerator StartWaving() {

        yield return new WaitForSeconds(currenOceanProperties.waveFrequency);

        GameObject go = Utility_Manager.instance.objectPoolManager.InstantiateFromPooler(waverPrefab);
        go.transform.parent = transform;
        go.transform.position = new Vector3(0, 0, 100);
        Waver waver = go.GetComponent<Waver>();
       
        waver.Initialize(currentWaverProperties, this);
        oldestWaver = waver;
        go.SetActive(true);

        StartCoroutine(StartWaving());
    }

    private Vector2[] GetUVs() {

        Vector2[] _uvs = new Vector2[(xSize + 1) * (zSize + 1)];
        int i = 0;
        for (int z = 0; z <= zSize; z += 1) {

            for (int x = 0; x <= xSize; x += 1) {

                _uvs[i] = new Vector2((float)x / xSize, (float)z / zSize);
                i++;
            }
        }
        return _uvs;
    }

    private void UpdateOceanView() {

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }

    private int[] GetTriangles() {

        int[] tries = new int[xSize * zSize * 6];

        int vertIndex = 0;
        int triIndex = 0;
        for (int z = 0; z < zSize; z++) {

            for (int x = 0; x < xSize; x++) {

                tries[triIndex + 0] = vertIndex + 0;
                tries[triIndex + 1] = vertIndex + xSize + 1;
                tries[triIndex + 2] = vertIndex + 1;
                tries[triIndex + 3] = vertIndex + 1;
                tries[triIndex + 4] = vertIndex + xSize + 1;
                tries[triIndex + 5] = vertIndex + xSize + 2;

                vertIndex++;
                triIndex += 6;
            }
            vertIndex++;
        }       

        return tries;
    }

    private Vector3[] GetVerticies() {

        Vector3[] verts = new Vector3[(xSize + 1) * (zSize + 1)];

        for (int i = 0, z = 0; z <= zSize; z++) {

            for (int x = 0; x <= xSize; x++) {

                verts[i++] = new Vector3(x, 0, z);
            }
        }                

        return verts;
    }

    private void CoolDownSee() {

        for (int i = 0, z = 0; z <= zSize; z++) {
            for (int x = 0; x <= xSize; x++) {

                //currentVerticies[i].x = Mathf.Lerp(currentVerticies[i].x, x, waveCoolDown);
                currentVerticies[i].y = Mathf.Lerp(currentVerticies[i].y, 0, currenOceanProperties.oceanCoolDown);
                currentVerticies[i].z = Mathf.Lerp(currentVerticies[i].z, z, currenOceanProperties.oceanCoolDown);
                i++;
            }
        }
    }
    
    void FixedUpdate() {

        CoolDownSee();
        //UpdateMaterial();
        mesh.vertices = currentVerticies;
        UpdateOceanView();
    }

    private void UpdateMaterial() {

        // Set Shore Line Visibility 
        thisMaterial.SetFloat("ShoreLineTransparency", Mathf.Lerp(3, .5f, transform.position.z));
    }

}
