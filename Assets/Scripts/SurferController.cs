﻿using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;

public class SurferController : MonoBehaviour
{
    public Animator anim;
    public float swimmingSpeed = 0.5f;

    SurferState surferState;
    float initializeTime;

    private void Awake() {

        //Initialize();
    }

    //public void Initialize() {

    //    //transform.position = initialPosition;
    //    transform.DORotate(new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z), 0.5f, RotateMode.Fast).OnComplete(() => {

    //        ChangeState(SurferState.SWIMMING);
    //    });
    //}

    public void Reset() {

        initializeTime = Time.time;
        transform.position = new Vector3(
            Random.Range(45, 50), 
            OceanManager.instance.surferInitialPosition.y, 
            OceanManager.instance.surferInitialPosition.z);

        anim.SetTrigger("ActiveSwimming");
        transform.DORotate(new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z), 0.5f, RotateMode.Fast).OnComplete(() => {

            surferState = SurferState.SWIMMING;
        });
    }

    private void FixedUpdate() {

        UpdatePosision();
    }

    private void UpdatePosision() {

        switch (surferState) {
            case SurferState.IDLE:
                break;
            case SurferState.SWIMMING:
                transform.Translate(0, 0, swimmingSpeed * Time.deltaTime);
                break;
            case SurferState.WATING:
                break;
            case SurferState.SURFING:
                break;
            default:
                break;
        }
    }

    public void ChangeState(SurferState surferState) {

        this.surferState = surferState;
        switch (surferState) {
            case SurferState.IDLE:
                break;
            case SurferState.SWIMMING:
                anim.SetTrigger("ActiveSwimming");
                break;
            case SurferState.WATING:
                break;
            case SurferState.SURFING:
                anim.SetTrigger("ActiveSurfing");
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter(Collider other) {

        if (other.gameObject.layer.Equals(Constant_Manager.ENEMY_LAYER)) {

            if (other.gameObject.CompareTag("AWave")) {

                if (other.GetComponent<Waver>().GetInitializeTime() < initializeTime) {

                    // This wave is created after this surfer.
                    // So this surfer will skip this wave
                    return;
                }
                transform.parent = other.transform;
            } 
            else if (other.gameObject.CompareTag("AWaveWarnner")){
                
                if (other.transform.parent.GetComponent<Waver>().GetInitializeTime() < initializeTime) {

                    // This wave warnner is created after this surfer.
                    // So this surfer will skip this wave
                    return;
                }
                //OceanObjects.instance.RemoveFlotingObject(transform);
                ChangeState(SurferState.SURFING);
                transform.DORotate(new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z), 0.5f, RotateMode.Fast);
            }
        }
    }

    //private void OnTriggerExit(Collider other) {
        
    //    if (other.gameObject.layer.Equals(Constant_Manager.ENEMY_LAYER)) {

    //        OceanObjects.instance.AddFlotingObject(other.transform);
    //        transform.parent = initialParent;
    //        ChangeState(SurferState.SWIMMING);
    //    }

    //}

}
