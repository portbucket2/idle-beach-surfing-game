﻿using System;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class OceanManager : MonoBehaviour
{
    public static OceanManager instance;
    public static UnityEvent OnCreatingWave, OnEndingWave;
    public static Action<BaseOceanProperties> OnPurchaseUpdateDone = delegate { };
    
    public OceanController oceanController;
    public UIManager uIManager;
    public Vector3 surferInitialPosition;

    BaseOceanProperties baseOceanProperties = new BaseOceanProperties();

    private void Awake() {

        instance = this;
        DOTween.Init();
        surferInitialPosition = new Vector3(0, 0, 0);
    }

    private void Start() {

        oceanController.InitializeOcean(GetSaveGameData());
    }

    private BaseOceanProperties GetSaveGameData() {

        #region GetSaveGameData
        // Try to fetch save data from here
        // If on data exist then send default data to ocean

        //Utility_Manager.instance.savefileManager.LoadFile();

        #endregion GetSaveGameData

        return baseOceanProperties;
    }

    public void OnUpgradeButtonPressed(ButtonType buttonType) {

        switch (buttonType.buttonTypes) {

            case ButtonTypes.HEIGHT:

                break;
            case ButtonTypes.SPEED:
                baseOceanProperties.waverProperties.waveSpeed += 2;
                baseOceanProperties.upgradableItems.waveSpeedUpgradeLevel++;
                break;
            case ButtonTypes.FREQUENCY:

                break;
            default:
                break;
        }

        baseOceanProperties.numberOfSurfer =
            baseOceanProperties.upgradableItems.waveHeightUpgradeLevel +
            baseOceanProperties.upgradableItems.waveSpeedUpgradeLevel +
            baseOceanProperties.upgradableItems.waveFrequencyUpgradeLevel;

        OnPurchaseUpdateDone(baseOceanProperties);
    }
}
