﻿using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public void OnUpdateButtonPressed(ButtonType buttonType) {

        OceanManager.instance.OnUpgradeButtonPressed(buttonType);
    }
}
