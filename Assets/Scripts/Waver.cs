﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Waver : MonoBehaviour
{
    WaverProperties waverProperties;
    OceanController oceanController;

    float initializeTime;

    public void Initialize(WaverProperties waverProperties, OceanController oceanController) {

        this.waverProperties = waverProperties;
        this.oceanController = oceanController;
        initializeTime = Time.time;
    }

    public float GetInitializeTime() {

        return initializeTime;
    }

    public WaverProperties GetWaverProperties() {

        return waverProperties;
    }

    private void FixedUpdate() {

        if (oceanController.Equals(null))
            return;

        UpdateWaver();
        UpdateWaveByWaver();
    }

    public void UpdateWaver() {

        transform.Translate(0, 0, Time.fixedDeltaTime * -waverProperties.waveSpeed);

        if (transform.position.z < 0) {

            Die();
        }
    }

    void UpdateWaveByWaver() {
        
        float currentHeight = waverProperties.waveHeight * transform.position.z - waverProperties.waveEndPoint;
        switch (waverProperties.waveType) {

            case WaveType.DYNAMIC:

                for (int i = (oceanController.xSize + 1) * (int)transform.position.z, z = 0; z < currentHeight * Mathf.PI; z++) {

                    for (int x = 0; x <= oceanController.xSize; x++) {

                        if (i < oceanController.currentVerticies.Length && i > 0) {

                            oceanController.currentVerticies[i].y = (Mathf.Sin(((z - (transform.position.z - (int)transform.position.z)) / currentHeight) - 1.5f) + 1) * currentHeight;
                            float f = oceanController.currentVerticies[i].y / (currentHeight * 2);
                            float p = Mathf.Lerp(0, 180, f);
                            //oceanController.currentVerticies[i].z = oceanController.originalVerticies[i].z + (waverProperties.frontCurve * currentHeight * Mathf.Sin(p * Mathf.Deg2Rad));
                            i++;
                        }
                    }
                }

                break;
            case WaveType.CUSTOM:

                for (int i = (oceanController.xSize + 1) * (int)transform.position.z, z = 0; z < waverProperties.waveScale * Mathf.PI * 2; z++) {

                    for (int x = 0; x <= oceanController.xSize; x++) {

                        if (i < oceanController.currentVerticies.Length && i > 0) {

                            oceanController.currentVerticies[i].y = (Mathf.Sin(((z - (transform.position.z - (int)transform.position.z)) / waverProperties.waveScale) - 1.5f) + 1) * currentHeight;
                            float f = oceanController.currentVerticies[i].y / (currentHeight * 2);
                            float p = Mathf.Lerp(0, 180, f);
                            oceanController.currentVerticies[i].z = oceanController.originalVerticies[i].z + (waverProperties.frontCurve * currentHeight * Mathf.Sin(p * Mathf.Deg2Rad));
                            i++;
                        }
                    }
                }

                break;
            default:
                break;
        }
    }

    private void Die() {

        List<Transform> surfers = new List<Transform>();
        foreach (Transform item in transform) {
            
            if (!item.gameObject.layer.Equals(Constant_Manager.PLAYER_LAYER))
                continue;
            surfers.Add(item);
        }

        while(surfers.Count > 0) {
            
            OceanObjects.instance.RemoveFlotingObject(surfers[0]);
            surfers.RemoveAt(0);
            //item.GetComponent<SurferController>().Reset();
        }

        Utility_Manager.instance.objectPoolManager.DestroyToPooler(gameObject);
    }

}