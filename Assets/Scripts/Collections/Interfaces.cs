﻿public interface IOceanObjects
{
    void InitializeObjects();
    void UpdatePosition();
    void UpdateRotation();
}