﻿[System.Serializable]
public struct WaverProperties {

    // Dynamic = Wave Height & Wave Scale values will be used
    // Custom = Wave Height value will be used only
    public WaveType waveType;

    public float 
        waveSpeed,
        waveHeight,
        waveScale,
        waveEndPoint,
        frontCurve;
}

public struct CurrenOceanProperties {

    public float
        waveFrequency,
        oceanCoolDown;

    public int
        xSize,
        zSize,
        numberOfSurfer,
        earningCoin,
        numberOfUpgrade;

    public WaverProperties waverProperties;
    public UpgradableItems upgradableItems;

    public CurrenOceanProperties(BaseOceanProperties baseOceanProperties) : this() {

        waveFrequency = baseOceanProperties.waveFrequency;
        oceanCoolDown = baseOceanProperties.oceanCoolDown;

        xSize = baseOceanProperties.xSize;
        zSize = baseOceanProperties.zSize;
        numberOfSurfer = baseOceanProperties.numberOfSurfer;
        earningCoin = baseOceanProperties.earningCoin;
        numberOfUpgrade = baseOceanProperties.numberOfUpgrade;

        waverProperties.waveType = baseOceanProperties.waverProperties.waveType;
        waverProperties.waveHeight = baseOceanProperties.waverProperties.waveHeight;
        waverProperties.waveScale = baseOceanProperties.waverProperties.waveScale;
        waverProperties.waveSpeed = baseOceanProperties.waverProperties.waveSpeed;
        waverProperties.waveEndPoint = baseOceanProperties.waverProperties.waveEndPoint;
        waverProperties.frontCurve = baseOceanProperties.waverProperties.frontCurve;

    }
}

public struct UpgradableItems {

    public int
        waveHeightUpgradeLevel,
        waveSpeedUpgradeLevel,
        waveFrequencyUpgradeLevel;
}

public class BaseOceanProperties {

    public float
        waveFrequency = 12f,
        oceanCoolDown = 0.01f;

    public int
        xSize = 60,
        zSize = 100,
        numberOfSurfer = 1,
        earningCoin = 0,
        numberOfUpgrade = 0;

    public WaverProperties waverProperties = new WaverProperties {

        waveType = WaveType.DYNAMIC,
        waveHeight = 0.1f,
        waveScale = 10,
        waveSpeed = 10,
        waveEndPoint = 0.5f,
        frontCurve = 1
    };
    public UpgradableItems upgradableItems = new UpgradableItems {

        waveHeightUpgradeLevel = 0,
        waveSpeedUpgradeLevel = 0,
        waveFrequencyUpgradeLevel = 0
    };

    public BaseOceanProperties() {

        oceanCoolDown /= (waveFrequency / 15);
    }

}