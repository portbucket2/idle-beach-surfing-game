﻿public enum WaveType {

    DYNAMIC,
    CUSTOM
}

public enum SurferState {

    IDLE,
    SWIMMING,
    WATING,
    SURFING
}

public enum ButtonTypes {

    HEIGHT,
    SPEED,
    FREQUENCY
}