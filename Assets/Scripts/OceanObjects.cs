﻿using System.Collections.Generic;
using UnityEngine;

public class OceanObjects : MonoBehaviour, IOceanObjects
{
    public static OceanObjects instance;

    [HideInInspector]
    public List<Transform> flotingObjects;
    [HideInInspector]
    public OceanController oceanController;

    Vector3[] meshNormals;

    private void Awake() {

        instance = this;
        InitializeObjects();
    }

    public virtual void InitializeObjects() {

        oceanController = OceanController.instance;
        flotingObjects = new List<Transform>();

        //for (int i = 0; i < transform.childCount; i++) {

        //    AddFlotingObject(transform.GetChild(i));
        //}
    }

    public void AddFlotingObject(Transform aNewFlotingObject) {

        aNewFlotingObject.parent = transform;
        flotingObjects.Add(aNewFlotingObject);
    }

    public void RemoveFlotingObject(Transform existingFlotingObject) {

        //flotingObjects.Remove(existingFlotingObject.GetInstanceID());
        existingFlotingObject.parent = null;
        Utility_Manager.instance.objectPoolManager.DestroyToPooler(existingFlotingObject.gameObject);
        oceanController.currentActiveSurfer--;

    }

    private void FixedUpdate() {

        if (oceanController == null)
            return;
        UpdatePosition();

        if(meshNormals == null) {

            meshNormals = oceanController.mesh.normals;
            return;
        }
        UpdateRotation();
    }

    public virtual void UpdatePosition() {

        try {

            //foreach (var item in flotingObjects) {

            //    if (item.Value.position.z < 0 || item.Value.position.z > oceanController.zSize)
            //        continue;

            //    Vector3 newPosition = new Vector3(
            //    item.Value.position.x,
            //    oceanController.currentVerticies[(oceanController.xSize + 1) * (int)item.Value.position.z].y,
            //    item.Value.position.z);

            //    item.Value.position = new Vector3(
            //        newPosition.x,
            //        Mathf.Lerp(item.Value.position.y, newPosition.y, 0.05f),
            //        newPosition.z
            //        );
            //}

            for (int i = 0; i < flotingObjects.Count; i++) {

                if (flotingObjects[i].position.z < 0 || flotingObjects[i].position.z > oceanController.zSize)
                    continue;

                Vector3 newPosition = new Vector3(
                flotingObjects[i].position.x,
                oceanController.currentVerticies[(oceanController.xSize + 1) * (int)flotingObjects[i].position.z].y,
                flotingObjects[i].position.z);

                flotingObjects[i].position = new Vector3(
                    newPosition.x,
                    Mathf.Lerp(flotingObjects[i].position.y, newPosition.y, 0.1f),
                    newPosition.z
                    );

            }

        } catch (System.Exception) {

            Debug.LogError("Position Warnning ");
        }        
    }

    public virtual void UpdateRotation() {
        
        try {

            for (int i = 0; i < flotingObjects.Count; i++) {

                if (flotingObjects[i].position.z < 0 || flotingObjects[i].position.z > oceanController.zSize)
                    continue;

                float angle = meshNormals[(oceanController.xSize + 1) * (int)flotingObjects[i].position.z].y * meshNormals[(oceanController.xSize + 1) * (int)flotingObjects[i].position.z].z * 45;
                flotingObjects[i].rotation = Quaternion.Euler(-angle, flotingObjects[i].eulerAngles.y, flotingObjects[i].eulerAngles.z);

            }

            foreach (var item in flotingObjects) {

                
            }

        } catch (System.Exception) {

            Debug.LogError("Rotation Warnning");
        }
    }
}
