﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class AISensorManager : MonoBehaviour {

    public Collider _collider;
    public List<GameObject> allClosestEnemyList = new List<GameObject>();

    private void Awake() {

        StartCoroutine(SortEnemyList());

    }

    private void OnTriggerEnter(Collider _player) {

        if (_player.gameObject.layer.Equals(11) || _player.gameObject.layer.Equals(23)) {

            allClosestEnemyList.Add(_player.gameObject);

        }
    }

    private void OnTriggerExit(Collider _player) {

        if (_player.gameObject.layer.Equals(11) || _player.gameObject.layer.Equals(23)) {

            StartCoroutine(RemoveSafely(_player));

        }
    }

    IEnumerator RemoveSafely(Collider _player) {

        while (sortingFlag)
            yield return null;

        allClosestEnemyList.Remove(_player.gameObject);
    }

    bool sortingFlag;

    //This function is responsible to know AI that
    //Someone is most close to you which is in your first index.
    public IEnumerator SortEnemyList() {

        yield return new WaitForSeconds(1);
        sortingFlag = true;

        yield return new WaitForEndOfFrame();

        if (allClosestEnemyList.Count >= 2) {

            try {
                allClosestEnemyList = allClosestEnemyList.OrderBy(
                    obj => (transform.position - obj.transform.position).sqrMagnitude
                   ).ToList();
            }
            catch (Exception e) {

                List<GameObject> temp = allClosestEnemyList;

                allClosestEnemyList.Clear();

                _collider.enabled = false;

                _collider.enabled = true;
            }
        }

        sortingFlag = false;
        StartCoroutine(SortEnemyList());
    }

    public Transform GetAEnemy() {

        if (sortingFlag.Equals(true))
            return null;

        if (allClosestEnemyList.Count <= 0)
            return null;

        if (allClosestEnemyList[0] == null) {

            allClosestEnemyList.RemoveAt(0);
            return null;

        }
        return allClosestEnemyList[0].transform;
    }

}
