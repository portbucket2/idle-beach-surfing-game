﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Chase")]
public class AliveChaseDecision : AIDecision {

    public override bool Decide(AIStateController stateController) {

        return IsTargetAlive(stateController);
    }

    private bool IsTargetAlive(AIStateController stateController) {

        if (stateController.chaseTarget == null)
            return false;

        return true;
    }
}
