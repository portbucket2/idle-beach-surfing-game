﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Scan")]
public class ScanDecision : AIDecision {

    public override bool Decide(AIStateController stateController) {

        return Scan(stateController);
    }

    private bool Scan(AIStateController stateController) {

        Transform newEnemy = stateController.aiSensorManager.GetAEnemy();

        if (newEnemy == null) {
            stateController.chaseTarget = null;
            return false;
        }

        stateController.chaseTarget = newEnemy;
        return true;
    }
}
