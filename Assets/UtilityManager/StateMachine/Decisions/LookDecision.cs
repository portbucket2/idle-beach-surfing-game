﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Look")]
public class LookDecision : AIDecision {

    public override bool Decide(AIStateController stateController) {

        return Look(stateController);
    }

    private bool Look(AIStateController stateController) {

        RaycastHit hit;

        Debug.DrawRay(
            stateController.transform.position,
            stateController.transform.forward.normalized * 10f,
            Color.green);

        if (Physics.SphereCast(
            stateController.transform.position,
            10f,
            stateController.transform.forward,
            out hit,
            10f)
            && hit.collider.gameObject.layer == 11) {   // 11 means player

            stateController.chaseTarget = hit.transform;
            return true;
        }

        return false;
    }
}
