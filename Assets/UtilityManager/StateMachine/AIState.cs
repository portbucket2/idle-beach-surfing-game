﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State")]
public class AIState : ScriptableObject {

    public AIAction[] aIActions;
    public AIStateTransitions[] aIStateTransitions;
    public Color stateColor = Color.gray;

	public void UpdateState(AIStateController stateController) {

        DoActions(stateController);
        CheckStateTransitions(stateController);
    }

    private void DoActions(AIStateController stateController) {

        for (int i = 0; i < aIActions.Length; i++) {

            aIActions[i].Act(stateController);
        }
    }

    void CheckStateTransitions(AIStateController stateController) {

        for (int i = 0; i < aIStateTransitions.Length; i++) {

            bool decisionSucceeded = aIStateTransitions[i].aIDecision.Decide(stateController);

            if (decisionSucceeded) {

                stateController.TransitionToNewState(aIStateTransitions[i].trueState);
            }
            else
                stateController.TransitionToNewState(aIStateTransitions[i].falseState);
        }

    }
}
