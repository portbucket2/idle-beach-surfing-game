﻿using System;

[Serializable]
public class AIStateTransitions{

    public AIDecision aIDecision;
    public AIState trueState;
    public AIState falseState;
}
