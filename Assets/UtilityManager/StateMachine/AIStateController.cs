﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AIStateController : MonoBehaviour {

    public NavMeshAgent navMeshAgent;
    public AISensorManager aiSensorManager;
    public AIState currentState;
    public AIState remainState;

    [HideInInspector] public bool aiActive;
    [HideInInspector] public Transform chaseTarget;
    public List<Transform> wayPoints;
    public Transform nextWayPoint;

    private void Awake() {

        navMeshAgent = GetComponent<NavMeshAgent>();
    }


    public void SetupAI(bool isAlive, List<Transform> wayPoints) {

        this.wayPoints = wayPoints;
        nextWayPoint = wayPoints[Random.Range(0, wayPoints.Count)];

        aiActive = isAlive;

        if (aiActive) {
            navMeshAgent.enabled = true;
        }
        else {
            navMeshAgent.enabled = false;
        }
    }

    private void Update() {

        if (!aiActive)
            return;

        currentState.UpdateState(this);
    }

    public void TransitionToNewState(AIState nextState) {

        if (!nextState.Equals(remainState)) {

            currentState = nextState;
        }
    }

    private void OnDrawGizmos() {

        Gizmos.color = currentState.stateColor;
        Gizmos.DrawWireSphere(transform.position, 2);
    }

}
