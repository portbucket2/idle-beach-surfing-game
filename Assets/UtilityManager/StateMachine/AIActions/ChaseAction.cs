﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/ChaseAction")]
public class ChaseAction : AIAction {

    public override void Act(AIStateController stateController) {

        Chase(stateController);
    }

    private void Chase(AIStateController stateController) {

        if (stateController.chaseTarget) {

            stateController.navMeshAgent.destination = stateController.chaseTarget.position;
            stateController.navMeshAgent.stoppingDistance = 0;
            stateController.navMeshAgent.isStopped = false;
        }
    }
}
