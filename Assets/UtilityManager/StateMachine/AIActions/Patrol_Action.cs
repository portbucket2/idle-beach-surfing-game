﻿using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Actions/PatrolAction")]
public class Patrol_Action : AIAction {

    public override void Act(AIStateController stateController) {

        Patrol(stateController);
    }

    private void Patrol(AIStateController stateController) {


        if (stateController.wayPoints.Count > 0) {

            stateController.navMeshAgent.stoppingDistance = Random.Range(0f, 3f);
            stateController.navMeshAgent.destination = stateController.nextWayPoint.position;
            stateController.navMeshAgent.isStopped = false;

            if (stateController.navMeshAgent.remainingDistance <= stateController.navMeshAgent.stoppingDistance
                && !stateController.navMeshAgent.pathPending) {

                stateController.nextWayPoint = stateController.wayPoints[Random.Range(0, stateController.wayPoints.Count)];
            }
        } 
        else {

            stateController.navMeshAgent.stoppingDistance = Random.Range(0f, 3f);
            stateController.navMeshAgent.destination = stateController.nextWayPoint.position;
            stateController.navMeshAgent.isStopped = false;
        }

    }

}
