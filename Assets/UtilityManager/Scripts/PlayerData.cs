﻿using UnityEngine;

public class PlayerData
{
    int health, level;
    float[] position;
  
    public PlayerData(Player player) {

        health = player.health;
        level = player.health;

        position = new float[3];
        position[0] = player.position.x;
        position[1] = player.position.y;
        position[2] = player.position.z;
    }
}
