﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is an auto resizeable dynamic pooler which can store multiple types of objects collection by its tag name. 
 * 
 */


using UnityEngine;
using System.Collections.Generic;

public class ObjectPool_Manager : MonoBehaviour
{
    Dictionary<string, Queue<GameObject>> poolDictionary = new Dictionary<string, Queue<GameObject>>();

    public GameObject InstantiateFromPooler(GameObject prefabObj) {

        GameObject obj;

        if ( !poolDictionary.ContainsKey(prefabObj.tag)) 
            poolDictionary[prefabObj.tag] = new Queue<GameObject>();

        if (poolDictionary[prefabObj.tag].Count <= 0) {

            obj = Instantiate(prefabObj);
            obj.SetActive(false);
            poolDictionary[prefabObj.tag].Enqueue(obj);
        }

        obj = poolDictionary[prefabObj.tag].Dequeue();
        //obj.SetActive(true);
        return obj;
    }

    public void DestroyToPooler(GameObject obj) {

        if(obj == null) {

            Utility_Manager.instance.SetMessage("A null object can not be store in pooler.");
            return;
        }

        if (!poolDictionary.ContainsKey(obj.tag))
            poolDictionary[obj.tag] = new Queue<GameObject>();

        obj.SetActive(false);
        poolDictionary[obj.tag].Enqueue(obj);
    }

}
