﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Manager : MonoBehaviour
{
    
    public void ShakeDefaultCamera(float duration = 0.04f, float magnitude = 0.02f) {

        StartCoroutine(ShakeDefaultCam(duration, magnitude));
    }

    IEnumerator ShakeDefaultCam(float duration, float magnitude) {

        Transform cam = Camera.main.transform;
        Vector3 initialPosition = cam.position;

        float elapseTime = 0.0f;

        while(elapseTime <= duration) {

            elapseTime += Time.deltaTime;

            Vector3 noise = new Vector3(
                Random.Range(-1f, 1f) * magnitude,
                Random.Range(-1f, 1f) * magnitude,
                Random.Range(-1f, 1f) * magnitude
                );

            noise.x += initialPosition.x;
            noise.y += initialPosition.y;
            noise.z += initialPosition.z;

            cam.transform.position = noise;

            yield return null;
        }

        cam.position = initialPosition;
    }


    bool isSlowMotionActive = false;
    float slowMotionDuration;
    public void DoSlowMotion(float duration = 2f, float magnitude = 0.05f) {

        if (isSlowMotionActive)
            return;

        slowMotionDuration = duration;
        Time.timeScale = magnitude;
        Time.fixedDeltaTime = Time.timeScale * 0.2f;
        isSlowMotionActive = true;
    }

    void UpdateSlowMotion() {

        if (isSlowMotionActive) {

            Time.timeScale += (1 / slowMotionDuration) * Time.unscaledDeltaTime;

            if (Time.timeScale > 1) {

                Time.timeScale = 1;
                isSlowMotionActive = false;
            }
        }
    }

    private void Update() {

        UpdateSlowMotion();
    }
}
