﻿public enum LoadLevelType {

    LOAD_BY_NAME,
    LOAD_BY_INDEX
}