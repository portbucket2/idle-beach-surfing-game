﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Savefile_Manager : MonoBehaviour
{
    public void SaveFile(Player player) {

        BinaryFormatter binaryFormatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/savefile.data";
        FileStream fileStream = new FileStream(path, FileMode.Create);

        PlayerData playerData = new PlayerData(player);

        binaryFormatter.Serialize( fileStream, playerData);
        fileStream.Close();
    }

    public PlayerData LoadFile() {

        string path = Application.persistentDataPath + "/savefile.data";
        if (File.Exists(path)) {

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(path, FileMode.Open);

            PlayerData playerData = binaryFormatter.Deserialize(fileStream) as PlayerData;
            fileStream.Close();

            return playerData;
        } 
        else {

            Debug.LogError("No savefile found at location: " + path);
            return null;
        }
    }
}
