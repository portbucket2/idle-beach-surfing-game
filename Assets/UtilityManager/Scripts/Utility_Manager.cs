﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is a manager which is responsible for common multi-tasking. 
 * like, Scene FadeIn-Out Transition, Camera shaking, Slowmotioning, Object Pooling etc.
 *  
 */

using UnityEngine;

public class Utility_Manager : MonoBehaviour {
    public static Utility_Manager instance;

    public bool debugMode = false;
    public Scene_Manager sceneManager;
    public Savefile_Manager savefileManager;
    public ObjectPool_Manager objectPoolManager;
    public Camera_Manager cameraManager;

    #region Singleton Pattern
    private void Awake() {

        if (instance != null) {

            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
        sceneManager.Initiallize();
    }
    #endregion  Singleton Pattern

    public void SetMessage(string msg) {

        if (debugMode)
            Debug.Log(msg);
    }
}