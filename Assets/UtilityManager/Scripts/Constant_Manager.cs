﻿using UnityEngine;

public class Constant_Manager : MonoBehaviour
{
    public const string SOUND = "SOUND";
    public const string MUSIC = "MUSIC";
    public const string CURRENT_SCORE = "CURRENT_SCORE";
    public const string HIGH_SCORE = "HIGH_SCORE";

    // Collision Layer ids. 
    //These layer's order must be maintained in Layer Array to work properly with Collision
    public const int PLAYER_LAYER = 8;
    public const int ENEMY_LAYER = 9;
    public const int GROUND_LAYER = 10;
    public const int BOUNDARY_LAYER = 11;
    public const int PICKUPS_LAYER = 12;

}
