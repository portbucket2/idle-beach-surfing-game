﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is a manager which is responsible for transitioning scene with cool fadein-out effect. 
 * 
 */

using UnityEngine.SceneManagement;
using UnityEngine;

public class Scene_Manager : MonoBehaviour
{
    Animator sceneFadeanimator;
    Animator SceneFadeanimator {

        get {

            if (sceneFadeanimator == null)
                sceneFadeanimator = GetComponent<Animator>();
            return sceneFadeanimator;
        }
    }
    string levelToLoadByName;
    int levelToLoadByIndex;

    LoadLevelType loadLevelType;

    public void Initiallize() {

        SceneManager.sceneLoaded += OnLoadCallback;
    }

    void OnLoadCallback(Scene scene, LoadSceneMode sceneMode) {

        Utility_Manager.instance.SetMessage(SceneManager.GetActiveScene().name + " Scene is loaded.");
        SceneFadeanimator.SetTrigger("FadeIn");
    }

    public void LoadLevel(string levelName) {

        levelToLoadByName = levelName;
        SceneFadeanimator.SetTrigger("FadeOut");
        loadLevelType = LoadLevelType.LOAD_BY_NAME;
    }

    public void LoadLevel(int levelIndex) {

        levelToLoadByIndex = levelIndex;
        SceneFadeanimator.SetTrigger("FadeOut");
        loadLevelType = LoadLevelType.LOAD_BY_INDEX;
    }

    public void FadeOutComplete() {

        switch (loadLevelType) {

            case LoadLevelType.LOAD_BY_NAME:

                SceneManager.LoadScene(levelToLoadByName);
                break;
            case LoadLevelType.LOAD_BY_INDEX:

                SceneManager.LoadScene(levelToLoadByIndex);
                break;
            default:
                break;
        }

    }
}
