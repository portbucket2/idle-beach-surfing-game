﻿using UnityEngine;

[System.Serializable]
public class Player : MonoBehaviour
{
    public int health;
    public int level;
    public Vector3 position;

}
