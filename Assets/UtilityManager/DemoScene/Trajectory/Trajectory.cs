﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Trajectory : MonoBehaviour
{
    public float velocity;
    public float angle;
    public int resolution;
    public float startLineThikness, endLineThikness;
    public Material lineMaterial;

    float gravity;
    float radianAngle;

    LineRenderer lr;

    private void Awake() {

        lr = GetComponent<LineRenderer>();
        lr.material = lineMaterial;
        lr.SetWidth( startLineThikness, endLineThikness);
        gravity = Mathf.Abs(Physics2D.gravity.y);
    }

    private void Update() {

        CreateTrajectory();
    }

    private void CreateTrajectory() {

        lr.positionCount = resolution + 1;

        Vector3[] points = CalculatePoints();

        for (int i = 0; i < points.Length; i++) {

            lr.SetPosition(i, points[i]);
        }
    }

    private Vector3[] CalculatePoints() {

        Vector3[] points = new Vector3[resolution + 1];
        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = Mathf.Pow(velocity, 2) * Mathf.Sin(2 * radianAngle) / gravity;

        for (int i = 0; i <= resolution; i++) {

            float deltaT = (float)i / (float)resolution;

            points[i] = CalculatePoint(deltaT, maxDistance);
        }

        return points;
    }

    private Vector3 CalculatePoint(float deltaT, float maxDistance) {

        Vector3 point;
        point.x = deltaT * maxDistance;
        point.y = point.x * Mathf.Tan(radianAngle) - ((gravity * point.x * point.x) / ( 2 * Mathf.Pow(velocity, 2) * Mathf.Pow( Mathf.Cos(radianAngle), 2)));
        point.z = 0;

        return point;
    }
}
