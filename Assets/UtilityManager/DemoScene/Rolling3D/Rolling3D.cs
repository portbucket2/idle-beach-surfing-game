﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Rolling3D : MonoBehaviour {

    [Range(1, 5)]
    [SerializeField] private int _maxIterations = 3;
    [SerializeField] private float _maxDistance = 10f;

    private int _count;
    private LineRenderer _line;
    private Rigidbody rig;

    private void Start() {
        _line = GetComponent<LineRenderer>();
        rig = GetComponent<Rigidbody>();
    }

    private void Update() {
        //_count = 0;
        //_line.SetVertexCount(1);
        //_line.SetPosition(0, transform.position);
        ////_line.enabled = RayCast(new Ray(transform.position, transform.forward));
        //RayCast(new Ray(transform.position, transform.forward));

        _count = 0;
        _line.positionCount = 1;
        _line.SetPosition(_count, transform.position);

        if (Input.GetMouseButtonDown(0)) {

            //rig.AddForce(transform.forward * (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position)).magnitude);
            rig.velocity = transform.forward * (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position)).magnitude * Time.deltaTime;
        }

        if (rig.velocity == Vector3.zero) {

            Angle();
            DrawLine(transform.position, transform.forward);
        }

    }

    void Angle() {

        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        pos = Input.mousePosition - pos;
        float angle = Mathf.Atan2(pos.x, pos.y) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle + 180, Vector3.up);
    }

    public void DrawLine(Vector3 startingPosition, Vector3 direction) {

        RaycastHit hit;

        if(Physics.Raycast(new Ray(startingPosition, direction), out hit, _maxDistance) && _count < _maxIterations){

            _count++;

            _line.positionCount++;
            _line.SetPosition(_count, hit.point);
            Vector3 reflection = Vector3.Reflect(direction, hit.normal);
            DrawLine(hit.point, reflection);
            return;
        }

        _line.positionCount += 2;
        _line.SetPosition(_count + 1, new Ray(startingPosition, direction).GetPoint(_maxDistance));
        return;

    }
}
