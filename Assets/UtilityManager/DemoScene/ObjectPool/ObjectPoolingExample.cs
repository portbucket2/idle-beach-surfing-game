﻿using UnityEngine;

public class ObjectPoolingExample : MonoBehaviour
{
    Utility_Manager UtilityManager;
    public GameObject prefabObj;
    public float CameraShakeDuration, CameraShakeMagnitude;

    GameObject go;

    private void Start() {

        UtilityManager = Utility_Manager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C)) {

            go = UtilityManager.objectPoolManager.InstantiateFromPooler(prefabObj);
            go.transform.position = new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), 0);

        } else if (Input.GetKeyDown(KeyCode.D)) {

            UtilityManager.objectPoolManager.DestroyToPooler(go);
            go = null;
        } else if (Input.GetKeyDown(KeyCode.S)) {

            UtilityManager.cameraManager.ShakeDefaultCamera(CameraShakeDuration, CameraShakeMagnitude);
        } else if (Input.GetKeyDown(KeyCode.R)) {

            UtilityManager.sceneManager.LoadLevel(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }
}
