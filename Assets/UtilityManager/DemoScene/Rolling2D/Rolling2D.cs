﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Rolling2D : MonoBehaviour
{
    [Range(1, 10)]
    public int maxTolarence = 3;
    public int maxDistance = 1;

    Rigidbody2D rig;
    LineRenderer line;

    int count;
    float magnitude;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        line = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        count = 0;
        line.positionCount = 1;
        line.SetPosition(count, transform.position);

        if (rig.velocity == Vector2.zero) {

            DrawLine(new Ray2D(transform.position, transform.up));
            CalculateAngle();

            if (Input.GetMouseButtonDown(0)) {

                rig.velocity = -transform.up * magnitude * Time.fixedDeltaTime * 5;
            }
        }

    }

    private void CalculateAngle() {

        Vector3 castValue = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 diffrence = Input.mousePosition - castValue;
        magnitude = diffrence.magnitude;

        float angle = Mathf.Atan2(diffrence.y, diffrence.x) * Mathf.Rad2Deg - 90;

        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, angle);        
    }

    private void DrawLine(Ray2D ray) {

        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, maxDistance);
        if( hit && count < maxTolarence) {
            
            count++;

            line.positionCount++;
            line.SetPosition(count, hit.point);
            Vector2 reflection = Vector2.Reflect(ray.direction, hit.normal);
            DrawLine(new Ray2D(hit.point, reflection));
            return;
        }

        line.positionCount++;
        line.SetPosition(count + 1, ray.GetPoint(maxDistance));
        return;
    }
}
